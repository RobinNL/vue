import { createRouter, createWebHistory } from 'vue-router'
import Form from '../pages/Form.vue'
import Data from '../pages/Data.vue'
import Home from '../pages/Home.vue'
import Basic from '../pages/Basic.vue'
import Animation from '../pages/Animation.vue'


const routerHistory = createWebHistory()

const router = createRouter({
    history: routerHistory,
    routes: [
        {
            path: '/',
            component: Home
        },
        {
            path: '/basic',
            component: Basic
        },
        {
            path: '/form',
            component: Form
        },
        {
            path: '/animation',
            component: Animation
        },
        {
            path: '/data',
            component: Data
        },

    ]
})

export default router;